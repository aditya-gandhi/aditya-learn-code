using System;
using System.Linq;
using System.Collections.Generic;

class GamePopularitySurvey
{
    static void Main(string[] args)
    {
        Dictionary<string, int> dataForSurvey = new Dictionary<string, int>();
        List<string> listOfNames = new List<string>();
        bool print = false;
        bool isFootballFavorite = false;

        var inputLine1 = Console.ReadLine().Trim();
        var numberOfPeopleForSurvey = Int32.Parse(inputLine1);
        for (var loopCounter = 0; loopCounter < numberOfPeopleForSurvey; loopCounter++)
        {
            var input = Console.ReadLine().Trim();
            string[] inputNameAndGame = input.Split(' ');

            if (!listOfNames.Contains(inputNameAndGame[0]))
            {
                if (dataForSurvey.ContainsKey(inputNameAndGame[1]))
                {
                    var v = dataForSurvey[inputNameAndGame[1]];
                    dataForSurvey[inputNameAndGame[1]] += 1;
                }
                else
                {
                    dataForSurvey.Add(inputNameAndGame[1], 1);
                }
            }
            listOfNames.Add(inputNameAndGame[0]);
        }

        var listsOfGameAndPopularity = from pair in dataForSurvey orderby pair.Value descending select pair;
        foreach (var listOfGameAndPopularity in listsOfGameAndPopularity)
        {
            if (!print)
            {
                Console.WriteLine(listOfGameAndPopularity.Key);
            }

            print = true;
            if (listOfGameAndPopularity.Key == "football")
            {
                Console.WriteLine(listOfGameAndPopularity.Value);
                isFootballFavorite = true;
            }
        }

        if (!isFootballFavorite)
        {
            Console.WriteLine("0");
        }
    }
}