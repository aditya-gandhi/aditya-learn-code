using System;
using System.Collections.Generic;

namespace FavoriteGameSurvey
{
    class FavoriteGameSurvey
    {
        static void Main(string[] args)
        {
            FixedSizeGenericHashTable<string, int> dataForSurvey = new FixedSizeGenericHashTable<string, int>(20);
            List<string> listOfNames = new List<string>();
            bool print = false;
            bool isFootballFavorite = false;

            var inputLine1 = Console.ReadLine().Trim();
            var numberOfPeopleForSurvey = Int32.Parse(inputLine1);
            for (var loopCounter = 0; loopCounter < numberOfPeopleForSurvey; loopCounter++)
            {
                var input = Console.ReadLine().Trim();
                string[] inputNameAndGame = input.Split(' ');

                if (!listOfNames.Contains(inputNameAndGame[0]))
                {
                    if (dataForSurvey.ContainsKey(inputNameAndGame[1]))
                    {
                        var v = dataForSurvey.Find(inputNameAndGame[1]);
                        dataForSurvey.Add(inputNameAndGame[1], v + 1);
                    }
                    else
                    {
                        dataForSurvey.Add(inputNameAndGame[1], 1);
                    }
                }
                listOfNames.Add(inputNameAndGame[0]);
            }

            dataForSurvey.PrintHashMap();
            /*  var listOfGameAndPopularity = from pair in dataForSurvey orderby pair.Value descending select pair;
                foreach (var o in listOfGameAndPopularity)
                {
                    if (!print)
                    {
                        Console.WriteLine(o.Key);
                    }

                    print = true;
                    if (o.Key == "football")
                    {
                        Console.WriteLine(o.Value);
                        isFootballFavorite = true;
                    }
                }

                if (!isFootballFavorite)
                {
                    Console.WriteLine("0");
                } */
        }
    }
}
