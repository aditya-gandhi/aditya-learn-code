﻿using System;
namespace BSTree
{
    public class HeightOfTree
    {
        static void Main(string[] args)
        {
            var inputNumberOfElementsArray = Console.ReadLine().Trim();
            long numberOfElementsInArray = Int32.Parse(inputNumberOfElementsArray);

            var elementsOfArrayInput = Console.ReadLine().Trim();
            string[] elementOfArray = elementsOfArrayInput.Split();
            int[] arrayToInsertInTree = Array.ConvertAll(elementOfArray, int.Parse);

            BST bst = new BST();

            int countOfHeightOfTree;
            int heightOfTree = 0;
            for (int i = 0; i < numberOfElementsInArray; i++)
            {
                countOfHeightOfTree = 0;
                bst.InsertNode(arrayToInsertInTree[i], ref bst.root, ref countOfHeightOfTree);
                if (countOfHeightOfTree > heightOfTree)
                {
                    heightOfTree = countOfHeightOfTree;
                }
            }
            Console.WriteLine(heightOfTree);
        }
        public class Node
        {
            public int data;
            public Node left;
            public Node right;
            public Node()
            {
            }
        }
        public class BST
        {
            public Node root;
            public BST()
            {
                root = null;
            }
            public void InsertNode(int data, ref Node Root, ref int count)
            {
                if (Root == null)
                {
                    Root = new Node();
                    Root.data = data;
                    count++;
                }
                else
                {
                    if (data <= Root.data)
                    {
                        count++;
                        InsertNode(data, ref Root.left, ref count);
                    }
                    else
                    {
                        count++;
                        InsertNode(data, ref Root.right, ref count);
                    }
                }
            }
            public void PrintTree(Node Root)
            {
                if (Root != null)
                {
                    PrintTree(Root.left);
                    Console.WriteLine(Root.data);
                    PrintTree(Root.right);
                }
            }
        }
    }
}