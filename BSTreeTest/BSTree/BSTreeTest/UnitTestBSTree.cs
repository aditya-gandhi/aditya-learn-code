﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using static BSTree.HeightOfTree;

namespace BSTree
{
    [TestClass]
    public class UnitTestBSTree
    {
        [TestMethod]
        public void TestInsertNode()
        {
            BST bst = new BST();
            int[] arrayToInsertInTree = new int[] { 1,2,3,4,5};
            int heightOfTree = 1;
            bst.InsertNode(arrayToInsertInTree[1], ref bst.root, ref heightOfTree);
            Assert.IsTrue(heightOfTree==2);
        }
    }
}
