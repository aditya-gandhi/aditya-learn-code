using System;

namespace FootballProblem
{
    /// <summary>
    /// Main class calling to functions. 
    /// </summary>
    public class Program
    {
        // Defining Constraints.
        const int maxTestCases = 100;
        const int maxPasses    = 100000;
        const int maxPlayerId  = 1000000;

        public static void Main(String[] args)
        {         
            FootballGame newFootballGame = new FootballGame();
           
            // Console.Write(" Enter total number of test cases: ");
            string testCase = Console.ReadLine();
            int testCases = int.Parse(testCase) <= maxTestCases ? int.Parse(testCase) : PrintError();

            while (testCases > 0)
            {
                newFootballGame.StartTheGame();

                for (int index = 1; index <= newFootballGame.numberOfPasses; index++)
                {
                    string[] passAndId;

                    // Console.Write(" To pass the ball press 'P' or 'p'...");
                    passAndId = Console.ReadLine().Split(' ');                              // Array have Pass action at {0} index and next player id at {1} index.
                    string pass = passAndId[0];

                    // Whether ball is passed or not.
                    if (pass == "P" || pass == "p")
                    {
                        int nextPlayerId = int.Parse(passAndId[1]);
                        newFootballGame.PassToNextPlayer(nextPlayerId);
                    }
                    else
                    {
                        newFootballGame.PassBack();
                    }
                }
                Console.WriteLine("Player " + newFootballGame.currentPlayer);
                testCases--;
            }
        }

        /// <summary>
        /// Performing actions of football game. 
        /// </summary>
        class FootballGame
        {
            public int previousPlayer, currentPlayer, numberOfPasses;

            // Constructor to initialize all the class variables.
            public FootballGame()
            {
                numberOfPasses = 0;                                                                    
                previousPlayer = 0;                                           
                currentPlayer  = 0;                                              
            }

            
            public void PassToNextPlayer(int PlayerId)
            {
                // Console.Write(" To whom you want to pass the ball? ");
                previousPlayer = currentPlayer;
                currentPlayer = PlayerId <= maxPlayerId ? PlayerId : PrintError();
            }

            /// <summary>
            /// Pass the ball to the previous player.
            /// </summary>
            public void PassBack()
            {
                int temp = currentPlayer;                                               
                currentPlayer = previousPlayer;
                previousPlayer = temp;
            }

            public void StartTheGame()
            {
                // Multiple line input, so brocken into two parts.
                string dualInput = Console.ReadLine();
                string[] passesAndPlayerId = dualInput.Split(' ');

                // Console.Write(" How many passes are going to happen? ");
                numberOfPasses = int.Parse(passesAndPlayerId[0]) <= maxPasses ? int.Parse(passesAndPlayerId[0]) : PrintError();

                // Console.Write(" Mention the player's number who is starting the game: ");
                currentPlayer = int.Parse(passesAndPlayerId[1]);
            }
        }

        public static int PrintError()
        {
            Console.Write("Invalid Value...");
            Console.Read();
            return 0;
        }
    }
}