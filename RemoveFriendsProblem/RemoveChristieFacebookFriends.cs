using System;
using System.Linq;

namespace RemoveChristieFacebookFriends
{
    class RemoveFriends
    {
        public FriendList head = null;
        public FriendList last = null;
        public static void Main(String[] args)
        {
            int noOfTestCases = Convert.ToInt32(Console.ReadLine());

            for (int j = 0; j < noOfTestCases; j++)
            {
                ChristieFriendList listOfFriends = new ChristieFriendList();

                int totalFriendsToBeDeleted = listOfFriends.FriendListInput();
                listOfFriends.RemoveFriends(totalFriendsToBeDeleted);
                listOfFriends.DisplayRemainingFriends();
            }
            Console.ReadLine();
        }
    }

    public class FriendList
    {
        private FriendList _next;
        private FriendList _prev;
        private int _data;

        public FriendList(int data, FriendList next, FriendList prev)
        {
            _data = data;
            _next = next;
            _prev = prev;
        }

        public FriendList()
        {
            _data = 0;
            _next = null;
            _prev = null;
        }

        public int data
        {
            get
            {
                return _data;
            }
            set
            {
                _data = value;
            }
        }

        public FriendList next
        {
            get
            {
                return _next;
            }
            set
            {
                _next = value;
            }
        }

        public FriendList prev
        {
            get
            {
                return _prev;
            }
            set
            {
                _prev = value;
            }
        }
    }

    public class ChristieFriendList
    {
        RemoveFriends friends = new RemoveFriends();
        public int FriendListInput()
        {
            FriendList current = null;
            var friendnumbers = Console.ReadLine().Trim().Split(' ').Select(int.Parse).ToArray();
            int totalfriends = friendnumbers[0];
            int totalFriendsToBeDeleted = friendnumbers[1];
            String[] Line = Console.ReadLine().Split(' ');
            int[] popularities = Array.ConvertAll(Line, int.Parse);

            foreach (var popularity in popularities)
            {
                if (friends.head == null)
                {
                    friends.head = new FriendList(popularity, current, null);
                    friends.last = friends.head;
                }
                else
                {
                    friends.last.next = new FriendList(popularity, null, null);
                    friends.last.next.prev = friends.last;
                    friends.last = friends.last.next;
                }
                current = friends.head;
            }
            return totalFriendsToBeDeleted;
        }

        public void RemoveFriends(int totalFriendsToBeDeleted)
        {
            bool flag = false;
            FriendList tempFriendList = new FriendList();
            tempFriendList = friends.head;
            while (totalFriendsToBeDeleted != 0)
            {
                if (tempFriendList.next == null)
                {
                    break;
                }
                else if ((tempFriendList.data) < (tempFriendList.next.data))
                {
                    if (tempFriendList.prev == null)
                    {
                        friends.head = tempFriendList.next;
                        friends.head.prev = null;
                        tempFriendList = tempFriendList.next;
                    }
                    else
                    {
                        tempFriendList.prev.next = tempFriendList.next;
                        tempFriendList.next.prev = tempFriendList.prev;
                        tempFriendList = tempFriendList.prev;
                    }
                    --totalFriendsToBeDeleted;
                    flag = true;
                }
                else
                {
                    tempFriendList = tempFriendList.next;
                }
            }
            if (flag == false)
            {
                friends.last = friends.last.prev;
                friends.last.next = null;
            }
        }

        public void DisplayRemainingFriends()
        {
            while (friends.head != null)
            {
                Console.Write("{0} ", friends.head.data);
                friends.head = friends.head.next;
            }
            Console.Write("\n");
        }
    }
}