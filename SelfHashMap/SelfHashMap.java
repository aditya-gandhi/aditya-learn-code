import java.util.*;

public class UseCustomHashMap<K, V>
{
    CustomHashMap<K, V> customHashMapAPI;

    public UseCustomHashMap()
    {
        customHashMapAPI = new CustomHashMap<K, V>();
    }

    public void addNewEntry(K k, V v)
    {
        customHashMapAPI.put(customHashMapAPI.getHashNode(k, v));
    }

    public void showAllEntries()
    {
        customHashMapAPI.printHashMap();
    }

    public void containsKey(K key)
    {
        if (customHashMapAPI.containsKey(key))
        {
            System.out.println("'" + key + "' Exists!!");
        }
        else
        {
            System.out.println("'" + key + "' Not Exists!!");
        }
    }

    public void removeEntry(K key)
    {
        if (customHashMapAPI.remove(key))
        {
            System.out.println("'" + key + "' Deleted!!");
        }
        else
        {
            System.out.println("'" + key + "' Not Deleted!!");
        }
    }

    public static void main(String[] args)
    {
        UseCustomHashMap<String, String> myHashMap = new UseCustomHashMap<String, String>();
        myHashMap.addNewEntry("A", "Apple");
        myHashMap.addNewEntry("B", "Ball");
        myHashMap.addNewEntry("C", "Cat");
        myHashMap.addNewEntry("D", "Dog");
        myHashMap.addNewEntry("E", "Egg");
        myHashMap.addNewEntry("F", "Fish");
        myHashMap.addNewEntry("G", "God");
        myHashMap.showAllEntries();
        myHashMap.containsKey("E");
        myHashMap.containsKey("CD");
        myHashMap.containsKey("G");
        myHashMap.remove("C");
        myHashMap.showAllEntries();
        myHashMap.remove("D");
    }
}

class HashNode<K, V>
{
    private K key;
    private V value;
    private int hashValue;

    HashNode(K key, V value)
    {
        this.key = key;
        this.value = value;
        this.hashValue = key.hashCode();
    }

    public K getKey()
    {
        return key;
    }

    public int getHashCodeOfKey()
    {
        return hashValue;
    }

    public V getValue()
    {
        return value;
    }
}

class CustomHashMap<K, V>
{
    private int sizeOfBucket;
    private ArrayList<LinkedList<HashNode<K,
    V>>> hashBucket;

    CustomHashMap()
    {
        sizeOfBucket = 16;
        hashBucket = new ArrayList<LinkedList<HashNode<K, V>>>(sizeOfBucket);
        for (int index = 0; index < getHashBucketSize(); index++)
        {
            hashBucket.add(index, null);
        }
    }

    CustomHashMap(int defaultSize)
    {
        sizeOfBucket = defaultSize;
        hashBucket = new ArrayList<LinkedList<HashNode<K, V>>>(sizeOfBucket);
        for (int index = 0; index < getHashBucketSize(); index++)
        {
            hashBucket.add(index, null);
        }
    }

    int getHashBucketSize()
    {
        return sizeOfBucket;
    }

    public HashNode<K,
    V> getHashNode(K key, V value)
    {
        return new HashNode<K, V>(key, value);
    }

    public void put(HashNode<K, V> hashNode)
    {
        int hashCode = (int)hashNode.getHashCodeOfKey();
        int index = hashCode & (getHashBucketSize() - 1);
        if (hashBucket.get(index) == null)
        {
            putWhenNull(hashCode, index, hashNode);
        }
        else
        {
            putWhenNotNull(hashCode, index, hashNode);
        }
    }

    public void putWhenNull(int hashCode, int index, HashNode<K, V> hashNode)
    {
        LinkedList<HashNode<K, V>> listOfHashNode = new LinkedList<HashNode<K, V>>();
        listOfHashNode.add(hashNode);
        hashBucket.remove(index);
        hashBucket.add(index, listOfHashNode);
    }

    public void putWhenNotNull(int hashCode, int index, HashNode<K, V> hashNode)
    {
        LinkedList<HashNode<K, V>> listForHashMap = hashBucket.get(index);
        if (!containsKey(hashNode.getKey()))
        {
            listForHashMap.add(hashNode);
        }
    }

    public boolean containsKey(K key)
    {
        int hashCode = key.hashCode();
        int indexAtHashBucket = hashCode & (getHashBucketSize() - 1);
        LinkedList<HashNode<K, V>> hashNodeListAtRequiredIndex = hashBucket.get(indexAtHashBucket);
        if (hashNodeListAtRequiredIndex != null)
        {
            for (HashNode<K, V> hashNode: hashNodeListAtRequiredIndex)
            {
                if (hashNode.getHashCodeOfKey() == hashCode && hashNode.getKey().equals(key))
                {
                    return true;
                }
            }
        }
        return false;
    }

    public Vget(K key)
    {
        int hashCode = key.hashCode();
        int indexAtHashBucket = hashCode & (getHashBucketSize() - 1);
        LinkedList<HashNode<K, V>> hashNodeListAtRequiredIndex = hashBucket.get(indexAtHashBucket);
        if (hashNodeListAtRequiredIndex != null)
        {
            for (HashNode<K, V> hashNode: hashNodeListAtRequiredIndex)
            {
                if (hashNode.getHashCodeOfKey() == hashCode && hashNode.getKey().equals(key))
                {
                    return hashNode.getValue();
                }
            }
        }
        return null;
    }

    public boolean remove(K key)
    {
        int hashCode = key.hashCode();
        int indexAtHashBucket = hashCode & (getHashBucketSize() - 1);
        LinkedList<HashNode<K, V>> hashNodeListAtRequiredIndex = hashBucket.get(indexAtHashBucket);
        if (hashNodeListAtRequiredIndex != null)
        {
            if (hashNodeListAtRequiredIndex.size() == 1)
            {
                return removeWhenOneHashNodeInListOfHashBucket(key, hashCode, hashNodeListAtRequiredIndex);
            }
            else
            {
                return removeWhenMultipleHashNodeInListOfHashBucket(key, hashCode, hashNodeListAtRequiredIndex);
            }
        }
        return false;
    }

    public boolean removeWhenOneHashNodeInListOfHashBucket(K key, int hashCode, LinkedList<HashNode<K, V>> linkedListInHashBucket)
    {
        HashNode<K, V> hashNode = linkedListInHashBucket.getFirst();
        if (hashNode.getHashCodeOfKey() == hashCode && hashNode.getKey().equals(key))
        {
            int indexOfHashBucket = hashCode & (getHashBucketSize() - 1); // could pass it as 4th argument in this function, would that be ok?
            hashBucket.remove(indexOfHashBucket);
            hashBucket.add(indexOfHashBucket, null);
            return true;
        }
        return false;
    }

    public boolean removeWhenMultipleHashNodeInListOfHashBucket(K key, int hashCode, LinkedList<HashNode<K, V>> linkedListInHashBucket)
    {
        int index = 0;
        for (HashNode<K, V> hashNode: linkedListInHashBucket)
        {
            if (hashNode.getHashCodeOfKey() == hashCode && hashNode.getKey().equals(key))
            {
                linkedListInHashBucket.remove(index);
                return true;
            }
            index++;
        }
        return false;
    }

    public void printHashMap()
    {
        for (LinkedList<HashNode<K, V>> hashNodeList: hashBucket)
        {
            if (hashNodeList != null)
            {
                for (int index = 0; index < hashNodeList.size(); index++)
                {
                    System.out.println(hashNodeList.get(index).getKey() + " --> " + hashNodeList.get(index).getValue());
                }
            }
        }
    }
}