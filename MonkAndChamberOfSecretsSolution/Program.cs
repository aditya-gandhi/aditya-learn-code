﻿using System;
using System.Collections.Generic;

namespace MonkAndChamberOfSecrets
{
    class Program
    {
        public static void Main(string[] args)
        {
            Monk monk = new Monk();

            monk.GetInputFromConsole();
            monk.CreateSpidersQueue();

            while (monk.iteration-- > 0)
            {
                monk.RemoveSpiders();
                monk.FindSpiderWithMaximumPower();
                monk.PrintSpiderPositionWithMaxiumPower();
                monk.AddRemovedSpidersBackToQueue();
            }
        }
    }

    public class Spider
    {
        public int position;
        public int power;
    }

    public class Monk
    {
        public int numberOfTotalSpiders, spidersToBeSelected, iteration;
        int[] powerOfSpiders;
        public Spider spiderWithMaxPower;
        public Spider spider = new Spider();
        List<Spider> tempListofSpiders = new List<Spider>();
        Queue<Spider> spiderQueue = new Queue<Spider>();

        public void GetInputFromConsole()
        {
            string inputString = Console.ReadLine();
            int[] spiderProperty = Array.ConvertAll(inputString.Split(' '), obj => int.Parse(obj));
            string inputString1 = Console.ReadLine();
            powerOfSpiders = Array.ConvertAll<string, int>(inputString1.Split(' '), obj => int.Parse(obj));
            GetCountOfTotalSpiders(inputString);
            numberOfTotalSpiders = IsNumberOfTotalSpidersValid(spiderProperty[0], 0, 316) ? spiderProperty[0] : PrintError();
            spidersToBeSelected = IsNumberOfSelectedSpiderValid(spiderProperty[1], numberOfTotalSpiders) ? spiderProperty[1] : PrintError();
            iteration = spidersToBeSelected;
        }

        public int[] GetPowerOfAllSpiders(string input)
        {
            return powerOfSpiders;
        }

        public int GetNumberOfSpidersToBeSelected(string inputString)
        {
            return spidersToBeSelected;
        }

        public int GetCountOfTotalSpiders(string inputString)
        {
            return numberOfTotalSpiders;
        }

        public void CreateSpidersQueue()
        {
            for (int index = 0; index < numberOfTotalSpiders; index++)
            {
                spiderQueue.Enqueue(new Spider()
                {
                    position = index + 1,
                    power = powerOfSpiders[index]
                });
            }
        }

        public void RemoveSpiders()
        {
            for (int i = 0; i < spidersToBeSelected; i++)
            {
                if (spiderQueue.Count == 0) break;
                var removedSpider = spiderQueue.Dequeue();
                tempListofSpiders.Add(removedSpider);
            }
        }

        public void FindSpiderWithMaximumPower()
        {
            spiderWithMaxPower = new Spider() { position = 0, power = -1 };
            for (int listItem = 0; listItem < tempListofSpiders.Count; listItem++)
            {
                if (spiderWithMaxPower.power < tempListofSpiders[listItem].power)
                {
                    spiderWithMaxPower = tempListofSpiders[listItem];
                }
            }
        }

        public void PrintSpiderPositionWithMaxiumPower()
        {
            Console.Write(spiderWithMaxPower.position + " ");
            tempListofSpiders.Remove(spiderWithMaxPower);
        }

        public void AddRemovedSpidersBackToQueue()
        {
            foreach (var spider in tempListofSpiders)
            {
                if (spider.power == 0)
                    spiderQueue.Enqueue(spider);
                else
                    spiderQueue.Enqueue(new Spider()
                    {
                        position = spider.position,
                        power = spider.power - 1
                    });
            }
            tempListofSpiders.Clear();
        }

        public bool IsNumberOfTotalSpidersValid(int numberOfSpiders, int lowerLimit, int upperLimit)
        {
            if (numberOfSpiders > lowerLimit || numberOfSpiders <= upperLimit)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public bool IsNumberOfSelectedSpiderValid(int numOfSpidersToSelect, int numberOfTotalSpiders)
        {
            if (numOfSpidersToSelect > numberOfTotalSpiders || numOfSpidersToSelect < Math.Pow(numberOfTotalSpiders, 2))
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public int PrintError()
        {
            Console.WriteLine("Invalid Input");
            return 0;
        }
    }
}